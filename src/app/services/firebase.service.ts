import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { Card } from '../models/Card';
import { Observable } from 'rxjs';
import { User } from '../models/User';
import { Adresse } from '../models/Adresse';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ){}

  getCards() : Observable<Card[]>{
          return this.afs.collection<Card>('Card').valueChanges();

  }

  // feature/fiche - AIT -->
  getCard(userKey) {
    return new Promise<Card>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.afs.doc<Card>('Card/' + currentUser.uid).valueChanges()
            .subscribe(snapshots => {
              console.log(snapshots);
              resolve(snapshots);
            }, err => {
              reject(err);
            });
        }
      });
    });
  }

  updateCard(userKey, value) {
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      this.afs.collection('Card').doc(currentUser.uid).set(value)
        .then(
          res => resolve(res),
          err => reject(err)
        );
    });
  }
  // feature/fiche - AIT <--

  /**
   *
   *
   *
   *
   *
   * VIEUX TEMPLATE
   *
   *
   *
   *
   *
   */

  getTasks(){
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if(currentUser){
          this.snapshotChangesSubscription = this.afs.collection('people').doc(currentUser.uid).collection('tasks').snapshotChanges();
          resolve(this.snapshotChangesSubscription);
        }
      })
    })
  }

  getTask(taskId){
    return new Promise<any>((resolve, reject) => {
      this.afAuth.user.subscribe(currentUser => {
        if(currentUser){
          this.snapshotChangesSubscription = this.afs.doc<any>('people/' + currentUser.uid + '/tasks/' + taskId).valueChanges()
          .subscribe(snapshots => {
            resolve(snapshots);
          }, err => {
            reject(err)
          })
        }
      })
    });
  }

  unsubscribeOnLogOut(){
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  updateTask(taskKey, value){
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('people').doc(currentUser.uid).collection('tasks').doc(taskKey).set(value)
      .then(
        res => resolve(res),
        err => reject(err)
      )
    })
  }

  deleteTask(taskKey){
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('people').doc(currentUser.uid).collection('tasks').doc(taskKey).delete()
      .then(
        res => resolve(res),
        err => reject(err)
      )
    })
  }

  createTask(value){
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('people').doc(currentUser.uid).collection('tasks').add({
        title: value.title,
        description: value.description,
        image: value.image
      })
      .then(
        res => resolve(res),
        err => reject(err)
      )
    })
  }

  createCard(card :Card){
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('Card').doc(currentUser.uid).set({
        id : card.id,
        user : card.user,
        name : card.name,
        coordinates : card.coordinates,
        description : card.description,
        address : card.address
      }).then(
        res => resolve(res),
        err => reject(err)
      )
    })
  }

  createUser(user :User){
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('Card').doc('User').collection('User').add({
        phoneNumber: user.phoneNumber,
        statut: user.statut,
        phoneComfirmed : user.isValidNumber
      }).then(
        res => resolve(res),
        err => reject(err)
      )
    })
  }

  createAdresse(adress :Adresse){
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('Card').doc('Addresse').collection('Addresse').add({
        numeroDeRue: adress.numeroDeRue,
        codePostal: adress.codePostal,
        adresse: adress.adresse,
        ville: adress.ville
      }).then(
        res => resolve(res),
        err => reject(err)
      )
    })
  }

  findUserByPhone(phoneNumber :string){
    return new Promise<any>((resolve, reject) => {
      var cardCollection = this.afs.collection("Card").ref;

      cardCollection.get().then((querySnapshot) => {

      querySnapshot.forEach((doc) => {
          if(doc.data().user.phoneNumber == phoneNumber){
            resolve(doc.data());
          }
      });

      }).catch((error) => {
        reject(error);
      })
    })
  }


  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux:any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL("image/jpeg");
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI, randomId){
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(randomId);
      this.encodeImageUri(imageURI, function(image64){
        imageRef.putString(image64, 'data_url')
        .then(snapshot => {
          snapshot.ref.getDownloadURL()
          .then(res => resolve(res))
        }, err => {
          reject(err);
        })
      })
    })
  }
}
