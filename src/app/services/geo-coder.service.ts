import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GeoCoderService {

  constructor(private http: HttpClient) { }

  callGouvApi(numero:string, adresse: String, postCode :string) {
    let urlApiGouv = "https://api-adresse.data.gouv.fr/search/";
    let adresseWithoutWitheSpace = adresse.replace(" ","+");
    let pathParams = "?q="+ numero +"+"+ adresseWithoutWitheSpace+"&postcode="+postCode;

    let completeUrl = urlApiGouv + pathParams;

    return this.http.get(completeUrl);

  }

  convertAdresseToGeoPoint(numero:string, adresse: String, postCode :string) : Promise<any>{
    return new Promise((resolve, reject) => {
      this.callGouvApi(numero, adresse, postCode).toPromise().then((result: any) => {
        let geoPoints = result.features[0].geometry.coordinates;
        resolve(geoPoints);
      }).catch((error) => {
        console.log("erreur dans le service geo-coder" + error)
        reject(error);
      })
    })

  }

}
