import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { FirebaseService } from './firebase.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLogged : BehaviorSubject<boolean> = new BehaviorSubject(false)

  constructor(
    private firebaseService: FirebaseService,
    public afAuth: AngularFireAuth
  ){

    this.afAuth.user.subscribe(user => {
      if(user){
        this.isLogged.next(true);
      }
    })

  }



  //Creation de compte avec email
  doRegister(value){

   return new Promise<any>((resolve, reject) => {
     firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
     .then(
       res => resolve(res),
       err => reject(err))
   })
  }

  doRegisterWithPhone(mobilePhone: string, appVerifier: firebase.auth.RecaptchaVerifier){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithPhoneNumber(mobilePhone, appVerifier)
      .then(
        res => resolve(res),
        err => reject(err))
    })
  }

  //Connexion avec email
  doLogin(value){
   return new Promise<any>((resolve, reject) => {
     firebase.auth().signInWithEmailAndPassword(value.email, value.password)
     .then(
       res => resolve(res),
       err => reject(err))
   })
  }

  doLogout(){
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signOut()
      .then(() => {
        this.firebaseService.unsubscribeOnLogOut();
        resolve();
      }).catch((error) => {
        console.log(error);
        reject();
      });
    })
  }
}
