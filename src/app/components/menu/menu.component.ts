import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {

  constructor(private menu: MenuController) {
  }

  toggleMenu() {
    this.menu.toggle().then(r => null);
  }

}
