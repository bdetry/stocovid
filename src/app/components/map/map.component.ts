import { Component, OnInit } from '@angular/core';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment
} from '@ionic-native/google-maps';
import { Platform } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  map: GoogleMap;

  constructor(private platform : Platform ,
              private firebaseService : FirebaseService ) { }

  ngOnInit() {


    this.platform.ready().then(x=>{
    this.loadMap();

    this.firebaseService.getCards().subscribe(cards => {

      cards.forEach(card=> {

        let  lat = card.coordinates.latitude;
        let  long = card.coordinates.longitude;

        let m = {
          title: card.name,
          icon: 'blue',
          animation: 'DROP',
          position: {
            lat: lat,
            lng: long
          }
        }
        this.setMarkerPoint(m);
      });
    });

    });
  }

  private setMarkerPoint(marker : MarkerOptions){

    let m: Marker = this.map.addMarkerSync(marker);

    m.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
      alert('clicked');
    });
  }

  loadMap() {

    // This code is necessary for browser
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyAD5pSYDeKxI97K_LCbkqhDdpUBkJBc3BA',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyAD5pSYDeKxI97K_LCbkqhDdpUBkJBc3BA'
    });

    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: 43.610767,
           lng: 3.876716
         },
         zoom: 18,
         tilt: 30
       }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);
  }
}

