import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirebaseService } from '../services/firebase.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  connexionForm: FormGroup;

  // formBuilder: FormBuilder;
  toast :ToastController;

  constructor(private formBuilder: FormBuilder, private firebaseService: FirebaseService, private router : Router) { }

  ngOnInit() {
    this.createForm();
  }

  private createForm(){
    this.connexionForm = this.formBuilder.group({
      phoneNumber: ["", Validators.required]
    })
  }

  submitForm(){
    //interroge la BDD
    this.firebaseService.findUserByPhone(this.connexionForm.get('phoneNumber').value)
    .then((result) =>{
      //si ok => redirection
      if(result){

        this.router.navigate(['/card'])

      }else{
        //sinon toast avec message
      }
    })
    .catch((error) => console.log("erreur lors de la requête pour vérifier si le num existe en bdd : " + error))





  }

}
