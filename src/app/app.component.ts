import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public isLogged : boolean = false

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public afAuth: AngularFireAuth,
    private auth : AuthService
  ) {
    this.initializeApp();
  }

  initializeApp() {
     this.platform.ready().then(() => {
       this.auth.isLogged.subscribe(logged=> {
        if(logged){
          this.isLogged = true
          this.router.navigate(['/enterprises'])
        }else{
          this.isLogged = false
          this.router.navigate(['/login'])
        }
       })
     });
  }

  logOut(){
    this.auth.doLogout().then(x=>
      this.router.navigate(['/login']))
  }
}
