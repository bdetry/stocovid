import { User } from "./User";
import { Adresse } from "./Adresse";

export interface Card {
    id : string;
    user : User;
    name : string;
    coordinates : firebase.firestore.GeoPoint;
    description : string;
    address : Adresse;
}
