export interface User {
    id : string;
    phoneNumber : string;
    isValidNumber : boolean;
    mail : string;
    statut: string;
}
