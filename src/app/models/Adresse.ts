export interface Adresse {
  numeroDeRue: string;
  codePostal: string;
  adresse: string;
  ville: string;
}
