import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import * as firebase from 'firebase/app';
import { AuthService } from '../services/auth.service';
import { User } from '../models/User';
import { FirebaseService } from '../services/firebase.service';
import { Adresse } from '../models/Adresse';
import { Card } from '../models/Card';
import { ToastController } from '@ionic/angular';
import { DOCUMENT } from '@angular/common';
import { GeoCoderService } from '../services/geo-coder.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.page.html',
  styleUrls: ['./subscription.page.scss'],
})
export class SubscriptionPage implements OnInit {

  @ViewChild(IonSlides) slides: IonSlides;

  windowRef: any;

  country: string;
  area: string;
  prefix: string;
  line: string;

  statut: string;
  streetNumber: string;
  rue: string;
  codePostal: string;
  ville: string;
  mobilePhone: string;
  confirmationCode: string;

  subscriptionForm: FormGroup;
  loading: boolean;
  smsSent: boolean;
  captchaValidated: boolean
  subscriptionSuccess: boolean = false;
  authService: AuthService;
  firebaseService: FirebaseService;

  slideOpts = {
    zoom: false
  };

  constructor(private formBuilder: FormBuilder,
    authService: AuthService,
    private router : Router,
    firebaseService: FirebaseService,
    public geoCoderService :GeoCoderService,
    public toastController: ToastController, @Inject(DOCUMENT) document) {
      this.windowRef = window.window;
      this.authService = authService;
      this.firebaseService = firebaseService
  }


  ngOnInit() {

    this.createForm();

    this.smsSent = false;
    this.captchaValidated = false

    firebase.auth().languageCode = 'fr';

    this.windowRef.captchaValidated = (response) => {
      if(response.lentgh > 0){
        return true
      }else{
        return false;
      }
    }

    //set le recaptcha, nécessaire pour validation par tel
    this.windowRef.recaptachVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'normal',
      'callback': function(response) {
        if(this.windowRef.grecaptcha.getResponse().length > 0){
          this.validate = this.captchaValidated(response);
        }
      }

    });
    this.windowRef.recaptachVerifier.render();
    this.slides.lockSwipes(true);
  }



  private createForm() {
    this.subscriptionForm = this.formBuilder.group({
      statut: ['', Validators.required],
      address: this.formBuilder.group({
        streetNumber: ['', [Validators.required,  Validators.min(0)]],
        rue: ['', Validators.required],
        codePostal: ['', [Validators.required,  Validators.min(0)]],
        ville: ['', Validators.required]
      }),
      mobilePhone: [''],
      confirmationCode: [''],
    });
  }

  /**
   * met la valeur des input du formulaire dans des variables
   */
  getFormValues() {
    this.statut = this.subscriptionForm.get('statut').value;
    this.streetNumber = this.subscriptionForm.get('address').get('streetNumber').value;
    this.rue = this.subscriptionForm.get('address').get('rue').value;
    this.codePostal = this.subscriptionForm.get('address').get('codePostal').value;
    this.ville = this.subscriptionForm.get('address').get('ville').value;
    this.mobilePhone = this.subscriptionForm.get('mobilePhone').value;
  }

  previousSlide(){
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

  nextSlideWithEnterKey(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }

  nextSlide(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }

  navigateToMap(){
    this.router.navigate(['/enterprises'])
  }

  submitForm(){

  }

  onlyNumberKey(evt) {

    // Only ASCII charactar in that range allowed
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)){
      let message = "chiffre seulement";
      this.presentToast(message);
    }
    return true;
  }


  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  sendSmsAuth(){
    if(this.subscriptionForm.get('mobilePhone').value.length < 12){
      let message = "merci de remplir correctement le numéro de téléphone"
      this.presentToast(message);
    }else if (this.windowRef.grecaptcha.getResponse().length <= 0 ){

      let message = "recaptcha non validé"
      this.presentToast(message);

    }else{
      this.loading=true; //affiche le spinner

      //cache le recaptcha
      let recaptcha = document.getElementById('recaptcha-container');
      recaptcha.style.visibility = 'hidden';

      const appVerifier = this.windowRef.recaptachVerifier;
      this.mobilePhone = this.subscriptionForm.get('mobilePhone').value;

      //envoi un texto
      this.authService.doRegisterWithPhone(this.mobilePhone, appVerifier)
      .then((result) => {
        this.loading = false; //cache le spinner
        this.smsSent = true; //affiche l'input pour rentrer le code
        this.windowRef.confirmationResult = result;
      }).catch((error) => {
        console.log(error);
        // let message = "merci de remplir tous les champs"
        // this.presentToast(error);
      })
    }
  }

  create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
  }

  //contrôle que le code envoyé par texto est le même que le code entré par l"utilisateur
   controlPhoneNumber(){

    this.windowRef.confirmationResult.confirm(this.subscriptionForm.get('confirmationCode').value)
    .then((result) => {

      this.subscriptionSuccess = true;
      let id = this.create_UUID();

      let user :User = {
        statut : this.statut,
        isValidNumber : true,
        phoneNumber : this.mobilePhone,
        id: id,
        mail: ''
      };

      let address: Adresse = {
        numeroDeRue: this.streetNumber.toString(),
        codePostal: this.codePostal,
        adresse: this.rue,
        ville: this.ville
      }

      this.geoCoderService.convertAdresseToGeoPoint(this.streetNumber, this.rue, this.codePostal).then((rawGeoPoint) =>{
        let geoPoint = new firebase.firestore.GeoPoint(rawGeoPoint[1],rawGeoPoint[0]);
        let card: Card = {
          id: id,
          user : user,
          name : 'nom temporaire',
          coordinates : geoPoint,
          description : 'description temporaire',
          address : address
        }

        //créer un user
        this.firebaseService.createCard(card).then(
          result => this.navigateToMap()
        ).catch((error) => console.log("erreur lors de l'enregistrement de l'utilisateur" + error));

      }).catch((error) => {
        this.presentToast("Est-ce que vous avez rentré une addresse correct ?");
        console.log("erreur lors de la conversion de l'addresse en geoPoint : " + error);
      })



    }).catch(error => console.log("erreur lors du control du code envoyé par sms" + error));

  }

}

