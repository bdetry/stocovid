import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastController} from '@ionic/angular';
import {FirebaseService} from '../services/firebase.service';
import {Card} from '../models/Card';
import {Adresse} from '../models/Adresse';
import {User} from '../models/User';

@Component({
  selector: 'app-card-config',
  templateUrl: './card-config.page.html',
  styleUrls: ['./card-config.page.scss'],
})
export class CardConfigPage implements OnInit {
  ficheForm: FormGroup;
  fiche: Card;
  private readonly adresse: Adresse;
  private readonly user: User;
  private userKey = 'a23d6e6a-081b-4ccc-ba82-8addca92bec5';

  constructor(private formBuilder: FormBuilder, private firebaseService: FirebaseService, public toastController: ToastController) {
    this.adresse = {
      numeroDeRue: null,
      adresse: null,
      ville: null,
      codePostal: null,
    };
    this.user = {
      id: null,
      phoneNumber: null,
      isValidNumber: null,
      mail: null,
      statut: null
    };
    this.fiche = {
      address: this.adresse,
      description: null,
      id: null,
      coordinates: null,
      user: this.user,
      name: null
    };
    // Create le formulaire de ficheForm
    this.ficheForm = this.formBuilder.group({
      name: [this.fiche.name, Validators.required],
      description: [this.fiche.description, Validators.required],
      numeroDeRue: [this.adresse.numeroDeRue, Validators.pattern('[0-9]*')],
      adresse: [this.adresse.adresse],
      ville: [this.adresse.ville],
      codePostal: [this.adresse.codePostal, Validators.pattern('[0-9]*')],
      phoneNumber: [this.user.phoneNumber, Validators.pattern('[0-9+ ]*')],
      email: [this.user.mail, Validators.email],
    });
  }

  sendForm() {
    const ficheUpdate = this.ficheForm.value;
    this.fiche.name = ficheUpdate.name;
    this.fiche.description = ficheUpdate.description;
    this.fiche.address.numeroDeRue = ficheUpdate.numeroDeRue;
    this.fiche.address.adresse = ficheUpdate.adresse;
    this.fiche.address.ville = ficheUpdate.ville;
    this.fiche.address.codePostal = ficheUpdate.codePostal;
    this.fiche.user.phoneNumber = ficheUpdate.phoneNumber;
    this.fiche.user.mail = ficheUpdate.email;
    // Send data to firebase
    this.firebaseService.updateCard(this.userKey, this.fiche).then(r => this.presentToast('Fiche bien enregistré 😀'));
  }

  ngOnInit() {
    this.firebaseService.getCard(this.userKey).then(r => this.getFiche(r));
  }

  async getFiche (r: Card) {
    if (r !== undefined) {
      this.fiche = r;
    } else {
      await this.presentToast('Fiche introuvable');
    }
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    await toast.present();
  }

}
