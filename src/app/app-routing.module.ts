import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPage } from './login/login.page';
import { EnterprisesPage } from './enterprises/enterprises.page';
import { SubscriptionPage } from './subscription/subscription.page';
import { CardConfigPage } from './card-config/card-config.page';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component : LoginPage },
  { path: 'enterprises', component : EnterprisesPage },
  { path: 'subscription', component : SubscriptionPage },
  { path: 'card', component : CardConfigPage }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
