// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyA2f_1-Ul_NamUM1V1dxgydpJNHZOn823w",
    authDomain: "stocovid.firebaseapp.com",
    databaseURL: "https://stocovid.firebaseio.com",
    projectId: "stocovid",
    storageBucket: "stocovid.appspot.com",
    messagingSenderId: "232760677842"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
