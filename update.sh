#!/bin/bash
cd /var/opt/stocovid || exit
git remote update
git fetch
if git merge-base --is-ancestor origin/master master; then
    echo Empty
else
    git checkout master
    git pull
    rm -R /var/www/stocovid/*
    cp -r www/* /var/www/stocovid/
fi
